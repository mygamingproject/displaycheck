﻿using UnityEngine;
using System.Collections;
using System;

public class GameManagement : MonoBehaviour
{
	public static GameManagement Instance { get; private set; }

	void Awake()
	{
		// First we check if there are any other instances conflicting
		if(Instance != null && Instance != this)
		{
			// If that is the case, we destroy other instances
			Destroy(gameObject);
		}
		
		// Here we save our singleton instance
		Instance = this;

		Application.targetFrameRate = 60;

		Debug.Log ("Pre setup: " + tk2dSystem.CurrentPlatform);	
		Debug.Log(Screen.width+" by "+Screen.height);

		if(Screen.width == 960.0f)
			tk2dSystem.CurrentPlatform = "1x"; 
		else if(Screen.width == 2048.0f)
			tk2dSystem.CurrentPlatform = "4x"; 
		else 
			tk2dSystem.CurrentPlatform = "2x"; 
		
		
		Debug.Log ("After Calculation: " + tk2dSystem.CurrentPlatform);		

		// Furthermore we make sure that we don't destroy between scenes (this is optional)
		DontDestroyOnLoad(gameObject);

		Application.LoadLevel("Alien");

		}




}


